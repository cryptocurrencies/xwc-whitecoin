# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/Whitecoin-org/whitecoin.git /opt/whitecoin 
RUN cd /opt/whitecoin/src && \
    make -j2 -f makefile.unix

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r whitecoin && useradd -r -m -g whitecoin whitecoin
RUN mkdir /data
COPY --from=build /opt/whitecoin/src/whitecoind /usr/local/bin/
RUN chown whitecoin:whitecoin /data
USER whitecoin
VOLUME /data
EXPOSE 22566 22565
CMD ["/usr/local/bin/whitecoind", "-datadir=/data", "-conf=/data/whitecoin.conf", "-server", "-txindex", "-printtoconsole"]